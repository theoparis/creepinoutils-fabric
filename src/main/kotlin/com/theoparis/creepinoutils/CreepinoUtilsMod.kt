package com.theoparis.creepinoutils

import net.fabricmc.api.ModInitializer
import org.apache.logging.log4j.LogManager


class CreepinoUtilsMod : ModInitializer {
    companion object {
        @JvmStatic
        val MOD_ID = "creepinoutils"

        @JvmStatic
        val logger = LogManager.getFormatterLogger(MOD_ID)

        // Define your registry items here (eg. blocks)

    }

    override fun onInitialize() {

    }
}
