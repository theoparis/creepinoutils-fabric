package com.theoparis.creepinoutils.util.tests

import net.fabricmc.api.EnvType
import net.fabricmc.loader.launch.knot.Knot
import net.fabricmc.loader.launch.knot.KnotClient
import net.fabricmc.loader.launch.knot.KnotServer
import net.fabricmc.loader.util.SystemProperties
import java.io.File

open class FabricTestHelper {
    fun startGame(environment: EnvType, args: Array<String> = arrayOf()) {
        val gameJarPath = System.getProperty(SystemProperties.GAME_JAR_PATH)
        if (environment == EnvType.CLIENT) KnotClient.main(args)
        else KnotServer.main(args)
    }

    fun stopGame() {

    }
}
