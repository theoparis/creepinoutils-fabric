import net.minecraft.client.util.math.Vector3f
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3i
import net.minecraft.block.Block
import net.minecraft.world.World
import net.minecraft.item.ItemStack;
import net.minecraft.entity.ItemEntity;
import net.minecraft.text.Text;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.client.util.math.Vector4f;
import net.minecraft.client.util.math.Vector3d;

fun String.toVec3i(): Vec3i {
    val x = this.split(",")[0].toInt()
    val y = this.split(",")[1].toInt()
    val z = this.split(",")[2].toInt()

    return Vec3i(x, y, z)
}

/**
 * Utility method to drop an itemstack in the world
 */
fun World.dropItem(item: ItemStack, pos: BlockPos) {
    if (!this.isClient)
        this.spawnEntity(
            ItemEntity(
                this,
                pos.x.toDouble(),
                pos.y.toDouble(),
                pos.z.toDouble(),
                item
            )
        )
}

fun World.getBlock(pos: BlockPos) = this.getBlockState(pos).block

fun PlayerEntity.sendMessage(text: Text) = this.sendMessage(text, false)

/**
 * Utility method to drop an block in the world
 */
fun World.dropBlock(block: Block, pos: BlockPos) =
    this.dropItem(ItemStack(block.asItem()), pos)

fun Vector3f.toVector4f() = Vector4f(this.x, this.y, this.z, 0f)
fun Vector4f.toVector3f() = Vector3f(this.x, this.y, this.z)
fun BlockPos.toVector3f() = Vector3f(this.x.toFloat(), this.y.toFloat(), this.z.toFloat())
fun BlockPos.toVector3d() = Vector3d(this.x.toDouble(), this.y.toDouble(), this.z.toDouble())
fun BlockPos.toVector3i() = Vec3i(this.x, this.y, this.z)
fun Vector3d.toBlockPos() = BlockPos(this.x, this.y, this.z)
fun Vec3i.toBlockPos() = BlockPos(this.x, this.y, this.z)
fun Vec3i.toVector3d() = Vector3d(this.x.toDouble(), this.y.toDouble(), this.z.toDouble())
