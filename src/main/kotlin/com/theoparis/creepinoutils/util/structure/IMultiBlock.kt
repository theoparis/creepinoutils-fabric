package com.theoparis.creepinoutils.util.structure

import net.minecraft.block.BlockState
import net.minecraft.util.math.BlockPos

interface IMultiBlock {
    fun getOtherParts(state: BlockState, pos: BlockPos): List<BlockPos>
}
