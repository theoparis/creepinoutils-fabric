package com.theoparis.creepinoutils.util.api

interface IExplosiveEntity {
    /**
     * Explosion fuse in ticks
     */
    var fuseTime: Int
    var fuseSpeed: Int
    fun ignite()
}
