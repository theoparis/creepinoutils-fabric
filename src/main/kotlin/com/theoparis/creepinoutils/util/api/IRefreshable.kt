package com.theoparis.creepinoutils.util.api

interface IRefreshable {
    fun refresh()
}
