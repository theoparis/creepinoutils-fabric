package com.theoparis.creepinoutils.util.api

import com.theoparis.creepinoutils.util.data.INBTSerializable
import net.minecraft.nbt.CompoundTag
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World
import java.io.Serializable

interface INetwork<T> : INBTSerializable<Any?, CompoundTag>, IRefreshable, Serializable {
    val world: World?
    override fun refresh()
    val connections: Set<BlockPos>
    fun merge(net: INetwork<T>)
    fun split(splitPoint: IBaseTile)
}
