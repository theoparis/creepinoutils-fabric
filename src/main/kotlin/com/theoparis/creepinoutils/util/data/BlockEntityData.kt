package com.theoparis.creepinoutils.util.data

import BlockData
import net.minecraft.block.BlockState
import net.minecraft.block.entity.BlockEntity
import net.minecraft.nbt.CompoundTag
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World

class BlockEntityData(world: World, pos: BlockPos, state: BlockState) : BlockData(world, pos, state) {
    var blockEntity: BlockEntity? = null

    override fun toTag(tag: CompoundTag): CompoundTag {
        val nbt = super.toTag(tag)
        if (blockEntity != null && !blockEntity!!.isRemoved)
            nbt.put("tile", blockEntity!!.toTag(CompoundTag()))

        return nbt
    }

    override fun fromTag(state: Any?, tag: CompoundTag): BlockEntityData {
        super.fromTag(state, tag)
        if (tag.contains("tile"))
            this.blockEntity = BlockEntity.createFromTag(this.state, tag.getCompound("tile"))
        return this
    }
}
