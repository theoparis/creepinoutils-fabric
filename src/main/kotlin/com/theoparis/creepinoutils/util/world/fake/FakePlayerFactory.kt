package com.theoparis.creepinoutils.util.world.fake


import com.google.common.collect.Maps
import com.mojang.authlib.GameProfile
import net.minecraft.nbt.CompoundTag
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld

/*
This is a direct copy of net/minecraftforge/common/util/FakePlayerFactory.java.
All credit to the forge team.
*/
//To be expanded for generic Mod fake players?
object FakePlayerFactory {
    // Map of all active fake player usernames to their entities
    val PLAYERS: MutableMap<GameProfile, FakeServerPlayer> = Maps.newHashMap()

    /**
     * Fake players must be unloaded with the world to prevent memory leaks.
     */
    // TOOD: world unload event
/*    fun unload(e: ServerWorldEvents.Unload) {
        unloadWorld(e.world)
    }*/

    fun copyFrom(original: ServerPlayerEntity): FakeServerPlayer {
        val fake = FakePlayerFactory[original.serverWorld, original.gameProfile]
        val nbt = original.toTag(CompoundTag())
        // Copy data from original player
        fake!!.toTag(nbt)
        return fake
    }

    /**
     * Get a fake player with a given username,
     * Mods should either hold weak references to the return value, or listen for a
     * WorldEvent.Unload and kill all references to prevent worlds staying in memory.
     */
    operator fun get(world: ServerWorld, username: GameProfile): FakeServerPlayer? {
        if (!PLAYERS.containsKey(username)) {
            val fakePlayer = FakeServerPlayer(world, username)
            PLAYERS[username] = fakePlayer
        }
        return PLAYERS[username]
    }

    fun unloadWorld(world: ServerWorld) {
        PLAYERS.entries.removeIf { entry: Map.Entry<GameProfile, FakeServerPlayer> -> entry.value.world === world }
    }
}
