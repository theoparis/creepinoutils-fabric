package com.theoparis.creepinoutils.util.world.fake

import com.mojang.authlib.GameProfile
import com.theoparis.creepinoutils.util.api.IFakePlayer
import net.minecraft.entity.damage.DamageSource
import net.minecraft.screen.NamedScreenHandlerFactory
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.network.ServerPlayerInteractionManager
import net.minecraft.server.world.ServerWorld
import net.minecraft.text.Text
import org.apache.commons.lang3.RandomStringUtils
import java.util.*

/**
 * This is a multiplayer fake player class for use in server-side mods.
 * Some credit goes to the forge team.
 */
class FakeServerPlayer @JvmOverloads constructor(
    world: ServerWorld,
    name: GameProfile? = GameProfile(
        UUID.randomUUID(),
        RandomStringUtils.random(6)
    )
) : ServerPlayerEntity(world.server, world, name, ServerPlayerInteractionManager(world)), IFakePlayer {
    // Disabled to prevent this method from calling the networkHandler methods
    override fun sendMessage(component: Text, actionBar: Boolean) {}

    // Allows for the position of the player to be the exact source when raytracing.
    override fun getEyeY(): Double {
        return this.pos.y
    }

    override fun openHandledScreen(factory: NamedScreenHandlerFactory?): OptionalInt {
        return OptionalInt.empty()
    }

    // Prevent this method from calling the networkHandler methods
    override fun teleport(targetWorld: ServerWorld?, x: Double, y: Double, z: Double, yaw: Float, pitch: Float) {
        this.refreshPositionAndAngles(x, y, z, yaw, pitch)
        setWorld(targetWorld)
    }

    override fun onDeath(source: DamageSource) {
        // Does not do anything atm, but this can be used in other fake player implementations
    }
}
