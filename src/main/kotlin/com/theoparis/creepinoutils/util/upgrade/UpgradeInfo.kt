package com.theoparis.creepinoutils.util.upgrade

import net.minecraft.item.ItemStack

data class UpgradeInfo(val upgrade: Upgrade, val upgradeItem: ItemStack)
