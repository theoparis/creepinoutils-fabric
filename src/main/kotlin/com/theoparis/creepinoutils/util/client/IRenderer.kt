import net.minecraft.client.render.VertexConsumerProvider
import net.minecraft.client.util.math.MatrixStack

interface IRenderer<T> {
    fun render(
            obj: T,
            yaw: Float,
            tickDelta: Float,
            matrices: MatrixStack,
            vertexConsumers: VertexConsumerProvider,
            light: Int
    )
}
