package com.theoparis.creepinoutils

import net.fabricmc.api.ClientModInitializer
import org.apache.logging.log4j.LogManager

class CreepinoUtilsClient : ClientModInitializer {
    companion object {
        @JvmStatic
        val logger = LogManager.getFormatterLogger(CreepinoUtilsMod.MOD_ID)

        // Client-side registration

    }

    override fun onInitializeClient() {
        // Client-side initialization
    }
}
