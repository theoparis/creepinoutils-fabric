package com.theoparis.creepinoutils.mixin;

import com.theoparis.creepinoutils.util.structure.IMultiBlock;
import net.minecraft.block.BedBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.HorizontalFacingBlock;
import net.minecraft.block.enums.BedPart;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.util.Collections;
import java.util.List;

@Mixin(BedBlock.class)
public class BedBlockMixin extends HorizontalFacingBlock implements IMultiBlock {
    @Shadow
    @Final
    public static EnumProperty<BedPart> PART;

    protected BedBlockMixin(Settings settings) {
        super(settings);
    }

    @Override
    public @NotNull List<BlockPos> getOtherParts(BlockState state, @NotNull BlockPos pos) {
        if (state.getBlock() != this) return Collections.emptyList();
        Direction facing = state.get(FACING); // HorizontalFacingBlock#FACING
        return state.get(PART) == BedPart.HEAD ? Collections.singletonList(pos.offset(facing.getOpposite())) : Collections.singletonList(pos.offset(facing));
    }
}

