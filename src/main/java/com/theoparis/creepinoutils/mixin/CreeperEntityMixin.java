package com.theoparis.creepinoutils.mixin;

import com.theoparis.creepinoutils.util.api.IExplosiveEntity;
import net.minecraft.entity.mob.CreeperEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(CreeperEntity.class)
public abstract class CreeperEntityMixin implements IExplosiveEntity {
    @Shadow
    private int fuseTime;

    @Shadow
    public abstract int getFuseSpeed();

    @Shadow
    public abstract void setFuseSpeed(int fuseSpeed);

    @Override
    public int getFuseTime() {
        return this.fuseTime;
    }

    @Override
    public void setFuseTime(int fuseTime) {
        this.fuseTime = fuseTime;
    }

    @Shadow
    public abstract void ignite();
}

