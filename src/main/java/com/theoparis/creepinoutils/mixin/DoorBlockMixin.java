package com.theoparis.creepinoutils.mixin;

import com.theoparis.creepinoutils.util.structure.IMultiBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.DoorBlock;
import net.minecraft.block.enums.DoubleBlockHalf;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.util.Collections;
import java.util.List;

@Mixin(DoorBlock.class)
public class DoorBlockMixin implements IMultiBlock {
    @Shadow
    @Final
    public static EnumProperty<DoubleBlockHalf> HALF;

    @Override
    public @NotNull List<BlockPos> getOtherParts(BlockState state, @NotNull BlockPos pos) {
        state.getBlock();
        return Collections.emptyList();
    }
}
